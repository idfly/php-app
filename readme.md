php docker image
================

General purpose php image by idfly.ru.

Contains:

  * extensions: mbstring, zip, pdo_mysql
  * packages: git, zlib1g-dev, ruby, ruby-dev
  * commands: composer
  * gems: bundler